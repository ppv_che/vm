package com.gk.training.task;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

public class VMTest {
    
    public VMImpl vm;
    
    @Before
    public void beforeInitialization() {
        vm = new VMImpl();
        Map<Item, Integer> map = new HashMap<Item, Integer>();
        for(int i = 0; i < 15; i++) {
            Item item = new Item(i, "name", i * 10, "");
            map.put(item, 20);
        }
        vm.reloadGoods(map);
        vm.reloadGoods(map);
    }
    
    @Test
    public void testCountOfItems() {
        int count = vm.getCountOfItem(new Item(1, "name", 10, ""));
        assertEquals(40, count);
    }
    
    @Test
    public void testSellProcess1() {
        Item item = new Item(5, "name", 50, "");

        vm.sellGoods(item);

        vm.updateMoneyInputContainer(51);
        
        int count = vm.getCountOfItem(item);
        assertEquals(39, count);
        
        int money = vm.getCountOfMoney();
        assertEquals(50, money);
        
        int moneyIn = vm.getMoneyInputContainer();
        assertEquals(0, moneyIn);
        
        int moneyOut = vm.getMoneyOutputContainer();
        assertEquals(1, moneyOut);
    }
    
    @Test
    public void testSellProcess2() {
        Item item = new Item(5, "name", 50, "");
        vm.updateMoneyInputContainer(51);
        vm.sellGoods(item);        
        
        int count = vm.getCountOfItem(item);
        assertEquals(39, count);
        
        int money = vm.getCountOfMoney();
        assertEquals(50, money);
        
        int moneyIn = vm.getMoneyInputContainer();
        assertEquals(0, moneyIn);
        
        int moneyOut = vm.getMoneyOutputContainer();
        assertEquals(1, moneyOut);
    }
    
    @Test
    public void testBusyFlag() {
        vm.updateMoneyInputContainer(5);
        
        boolean busy = vm.isBusy();
        assertTrue(busy);
        
        vm.cancel();
        
        busy = vm.isBusy();
        assertFalse(busy);
    }
    
    @Test
    public void tesEncashment() {
        Item item = new Item(9, "name", 90, ""); 
        vm.updateMoneyInputContainer(970);
        vm.sellGoods(item);
        vm.encashment();
        
        int money = vm.getCountOfMoney();
        assertEquals(20, money);
        
        int moneyOut = vm.getMoneyOutputContainer();
        assertEquals(880, moneyOut);
    }
    
}

package com.gk.training.task;

import java.util.HashMap;
import java.util.Map;

import com.gk.training.task.comm.Callee;

/**
 * Vending machine realization
 * @author pprodan
 */
public class VMImpl implements VM, Callee {
    
    private static int MIN_FOR_CHANGE = 20;
    
    private int money;
    
    private Map<Item, Integer> mapItems;
    
    private int moneyInputContainer;
    
    private int moneyOutputContainer;
    
    //next two fields for put money after choose some item they keep temporary data
    private boolean busy;
    
    private Item tempItem;
    
    public VMImpl() {
        mapItems = new HashMap<Item, Integer>();
    }
    
    public int getCountOfMoney() {
        return this.money;
    }
    
    public int getCountOfItem(Item item) {
        return this.mapItems.get(item);
    }

    public void sellGoods(Item item) {
        if (this.mapItems.get(item) > 0) {
            this.busy = true;
            //two modes depend on sequencing
            if (this.moneyInputContainer >= item.getPrice()) {
                this.sell(item);
            } else {
                this.tempItem = item;
            }
        } else {
            System.err.println("VM doesn't have this item");
        }
    }
    
    /**
     * method give possibility to set timeout of maximum busy time of VM
     * put it in else in sellGoods but it should call setter from other thread
     * @param item
     */
    private void waitMoney(Item item) {
        for(int i = 0; i < 180; i++) {
            try {
                System.err.println("Not enough credit!");
                Thread.sleep(1000);
                if (this.moneyInputContainer >= item.getPrice()) {
                    this.sell(item);
                    return;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.err.println("Time's up");
        this.cancel();
    }
    
    private void sell(Item item) {
        System.err.println("Bon Appetit!");
        this.mapItems.put(item, this.mapItems.get(item) - 1);
        this.moneyOutputContainer = this.moneyInputContainer - item.getPrice();
        this.moneyInputContainer = 0;
        this.money += item.getPrice();
        this.busy = false;
        this.tempItem = null;
    }
    
    public void reloadGoods() {
        for(Map.Entry<Item, Integer> entry : this.mapItems.entrySet()) {
            entry.setValue(20);
        }
    }

    public void reloadGoods(Map<Item, Integer> mapItems) {
        for(Map.Entry<Item, Integer> entry: mapItems.entrySet()) {
            Item item = entry.getKey();
            int amount = entry.getValue();
            if (this.mapItems.containsKey(item)) {
                this.mapItems.put(item, (amount + this.mapItems.get(item)));
            } else {
                this.mapItems.put(item, amount);
            }
        }
    }

    public void encashment() {
        if (this.money > MIN_FOR_CHANGE) {
            this.money = MIN_FOR_CHANGE;
        } else {
            System.err.println("Amount of money in VM is less than minimum required amount for change");
        }
    }
    
    public void cancel() {
        this.moneyOutputContainer = this.moneyInputContainer;
        this.moneyInputContainer = 0;
        this.busy = false;
    }

    public void setMoneyOutputContainer(int moneyOutputContainer) {
        this.moneyOutputContainer = moneyOutputContainer;
    }

    public int getMoneyOutputContainer() {
        return moneyOutputContainer;
    }

    public static int getMIN_FOR_CHANGE() {
        return MIN_FOR_CHANGE;
    }

    public static void setMIN_FOR_CHANGE(int min_for_change) {
        MIN_FOR_CHANGE = min_for_change;
    }
    
    public boolean isBusy() {
        return this.busy;
    }
    
    public int getMoneyInputContainer() {
        return moneyInputContainer;
    }
    
    /**
     * method that give possibility put money into input container from POS
     */
    public void updateMoneyInputContainer(int moneyInputContainer) {
        if (moneyInputContainer > 0) {
            this.busy = true;
            this.moneyInputContainer += moneyInputContainer;
            if ((this.tempItem!=null)&&(this.tempItem.getPrice()<=this.moneyInputContainer)) {
                this.sell(this.tempItem);
            }
        } else {
            System.err.println("Incorrect data");
        }
    }
    
    public void removeMoneyInputContainer() {
        this.money += this.moneyInputContainer;
        this.moneyInputContainer = 0;
    }
}

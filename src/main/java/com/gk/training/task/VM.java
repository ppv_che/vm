package com.gk.training.task;

import java.util.Map;

public interface VM {
    
    public int getCountOfMoney();
    
    public int getCountOfItem(Item item);
    
    public void sellGoods(Item item);
    
    public void reloadGoods();
    
    public void reloadGoods(Map<Item, Integer> mapItems);
    
    public void encashment();
    
    public void cancel();
    
    public boolean isBusy();

}
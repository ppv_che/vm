package com.gk.training.task.comm;

public interface Callee {
    
    public int getMoneyInputContainer();
    
    public void updateMoneyInputContainer(int moneyInputContainer);
    
    public void removeMoneyInputContainer();
    
    public boolean isBusy();
    
}

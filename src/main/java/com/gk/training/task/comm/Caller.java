package com.gk.training.task.comm;

public interface Caller {
    
    public void setCallee(Callee callee);
    
    public boolean sendMoneyToCallee(int money);
    
    public int getMoneyFromCalleeInputContainer();
    
    public void removeMoneyFromCalleeInputContainer();
    
    public boolean isCalleeBusy();
    
}
